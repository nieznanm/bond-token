#CC=solc
MAPPED_VOLUME=/sources
CC=docker run -v $(PWD):$(MAPPED_VOLUME) ethereum/solc:0.5.16
PATH_REMAPPING=@openzeppelin=$(MAPPED_VOLUME)/node_modules/@openzeppelin
CFLAGS=--abi --bin --metadata --metadata-literal --overwrite --combined-json abi,bin
BUILD_DIR=build
SOURCE_DIR=src
STAGING_DIR=staging

MAPPED_BUILD_DIR=$(MAPPED_VOLUME)/$(BUILD_DIR)
MAPPED_SOURCE_DIR=$(MAPPED_VOLUME)/$(SOURCE_DIR)

MAPPED_SOURCES=$(MAPPED_SOURCE_DIR)/contract.sol



TARGET_ARTIFACTS=$(BUILD_DIR)/BondToken.abi $(BUILD_DIR)/BondToken.bin $(BUILD_DIR)/BondToken_meta.json

bond_token:
	npm ci
	mkdir -p $(BUILD_DIR) $(STAGING_DIR)
	$(CC) $(PATH_REMAPPING) $(MAPPED_SOURCES) $(CFLAGS) -o $(MAPPED_BUILD_DIR)
	cp $(TARGET_ARTIFACTS) $(STAGING_DIR)
