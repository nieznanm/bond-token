pragma solidity ^0.5.0;

import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol";

contract BondToken is ERC20Mintable, ERC20Detailed {
  constructor() ERC20Detailed("BondTokenV0", "BTV0", 0) public {
  }
}
