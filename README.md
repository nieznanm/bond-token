# BondToken
BondToken is a mintable ERC20 token.

# Compile
```
make
```

The bytecode and a .json file containing metadata and abi will
be put in the `staging` directory which will be created if it
doesn't exist.
